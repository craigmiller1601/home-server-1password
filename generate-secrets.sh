#!/usr/bin/env bash

set -euo pipefail

CREDS_FILE=./1password-credentials.json
TOKEN_FILE=./1password-token.txt
TEMP_DIR=./temp
TEMP_TOKEN_FILE="$TEMP_DIR/token.yaml"
TEMP_CREDS_FILE="$TEMP_DIR/creds.yaml"
TEMPLATES_DIR=./deploy/chart/templates
CREDS_SECRET="$TEMPLATES_DIR/onepassword-credentials-json.yaml"
TOKEN_SECRET="$TEMPLATES_DIR/onepassword-token.yaml"

clean() {
  if [ -d "$TEMP_DIR" ]; then
    rm -rf "$TEMP_DIR"
  fi
}

prepare() {
  if [ ! -d "$TEMP_DIR" ]; then
    mkdir "$TEMP_DIR"
  fi
}

gen_raw_secrets() {
  if [ ! -f "$TOKEN_FILE" ]; then
    echo "Missing $TOKEN_FILE" >&2
    exit 1
  fi

  if [ ! -f "$CREDS_FILE" ]; then
    echo "Missing $CREDS_FILE" >&2
    exit 1
  fi

  token=$(cat "$TOKEN_FILE")
  creds=$(base64 < "$CREDS_FILE")

  kubectl create secret generic onepassword-token \
    -n secret-management \
    --from-literal=token="$token" \
    --dry-run=client -o yaml > "$TEMP_TOKEN_FILE"

  kubectl create secret generic op-credentials \
    -n secret-management \
    --from-literal=1password-credentials.json="$creds" \
    --dry-run=client -o yaml > "$TEMP_CREDS_FILE"
}

seal_secrets() {
  kubeseal \
    --controller-name=sealed-secrets-controller \
    --controller-namespace=kube-system \
    --format=yaml \
    < "$TEMP_TOKEN_FILE" \
    > "$TOKEN_SECRET"

  kubeseal \
    --controller-name=sealed-secrets-controller \
    --controller-namespace=kube-system \
    --format=yaml \
    < "$TEMP_CREDS_FILE" \
    > "$CREDS_SECRET"
}

clean
prepare
gen_raw_secrets
seal_secrets
clean

