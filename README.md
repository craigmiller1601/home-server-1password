# Home Server 1Password

Integrates the 1Password Connect Service & Operator into the Kubernetes cluster.

## Forked Chart

This repo uses a fork of the 1Password helm chart: https://github.com/craigmiller160/connect-helm-charts

This allows for self-signed TLS certificates internally. Hopefully the PR will be accepted eventually upstream.

## Secrets

The 1Password token and credentials are provided via sealed secrets. To re-create them, do the following:

1. Create a `1password-token.txt` file with only the token value, no blank lines
2. Create a `1password-credentials.json` file with only the credentials JSON base64 encoded, no blank lines.
3. Run `generate-secrets.sh`.
4. Commit and push the changes.